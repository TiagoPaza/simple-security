import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

@Component({
    selector: "ns-settings",
    templateUrl: "./settings.component.html",
    moduleId: module.id
})
export class SettingsComponent implements OnInit {

    constructor() {
        //
    }

    ngOnInit() {
        //
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
