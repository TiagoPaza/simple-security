import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

@Component({
    selector: "ns-keyboard",
    templateUrl: "./keyboard.component.html",
    styleUrls: ["./keyboard.component.scss"],
    moduleId: module.id
})
export class KeyboardComponent implements OnInit {
    private operation: string = "";
    private result: string = "";

    constructor() {
        //
    }

    ngOnInit(): void {
        //
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    private append(element: string){
        this.operation += element;
    }

    private undo() {
        if (this.operation !== '') {
            this.operation = this.operation.slice(0, -1);
        }
    }

    private clear() {
        this.operation = "";
    }

    private evaluate() {
        try {
            this.result = eval(this.operation);
        } catch(e){
            // dialogs.alert({title: 'Error', message: 'Cannot evaluate expression!', okButtonText: 'OK'});
        }
    }
}
