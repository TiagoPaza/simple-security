import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { KeyboardRoutingModule } from "~/app/menu/keyboard/keyboard-routing.module";
import { KeyboardComponent } from "./keyboard.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        KeyboardRoutingModule
    ],
    declarations: [
        KeyboardComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class KeyboardModule { }
