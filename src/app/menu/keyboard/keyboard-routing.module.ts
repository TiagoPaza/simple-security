import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { KeyboardComponent } from "./keyboard.component";

const routes: Routes = [
    {path: "", component: KeyboardComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class KeyboardRoutingModule {
}
