import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { CardView } from "nativescript-cardview";
import { MapView, Marker, Polyline, Position } from "nativescript-google-maps-sdk";
import { Color } from "tns-core-modules/color";

const geolocation = require("nativescript-geolocation");

import { registerElement } from "nativescript-angular/element-registry";
import * as app from "tns-core-modules/application";

registerElement("MapView", () => MapView);
registerElement("CardView", () => CardView);

const style = require("./map-style.json");

@Component({
    selector: "ns-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"],
    moduleId: module.id
})

export class HomeComponent implements OnInit {
    data = [];
    systemActive = false;

    mapView: any = null;
    watchId: number = null;
    gpsLine: Polyline;
    tapLine: Polyline;
    tapMarker: any;
    gpsMarker: any;
    centeredOnLocation: boolean = false;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        //
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    enableLocation() {
        if (!geolocation.isEnabled()) {
            console.log("Location not enabled, requesting.");

            return geolocation.enableLocationRequest();
        } else {
            return Promise.resolve(true);
        }
    }

    getLocation() {
        if (geolocation.isEnabled()) {
            return geolocation.getCurrentLocation({
                desiredAccuracy: 10,
                updateDistance: 10,
                minimumUpdateTime: 1000,
                maximumAge: 10000
            });
        }

        return Promise.reject("Geolocation not enabled.");
    }

    onMapReady(e) {
        if (this.mapView || !e.object) {
            return;
        }

        this.mapView = e.object;
        this.mapView.setStyle(style);

        this.enableLocation()
            // .then(this.getLocati0on)
            .then(() => {
                this.watchId = geolocation.watchLocation(this.locationReceived, this.error, {
                    desiredAccuracy: 10,
                    updateDistance: 10,
                    minimumUpdateTime: 10000,
                    maximumAge: 60000
                });
            }, this.error);
    }

    mapTapped = (event) => {
        this.removeMarker(this.tapMarker);
    }

    locationReceived = (position: Position) => {
        if (this.mapView && position && !this.centeredOnLocation) {
            this.mapView.latitude = position.latitude;
            this.mapView.longitude = position.longitude;
            this.mapView.zoom = 16;
            this.centeredOnLocation = true;
        }

        this.removeMarker(this.gpsMarker);
        this.gpsMarker = this.addMarker({
            location: position,
            title: "Minha casa"
        });
    }

    addMarker(args: AddMarkerArgs) {
        if (!this.mapView || !args || !args.location) {
            return;
        }

        const marker = new Marker();
        marker.position = Position.positionFromLatLng(args.location.latitude, args.location.longitude);
        marker.title = args.title;
        marker.snippet = args.title;
        this.mapView.addMarker(marker);

        return marker;
    }

    removeMarker(marker: Marker) {
        if (this.mapView && marker) {
            this.mapView.removeMarker(marker);
        }
    }

    error(err) {
        console.log("Error: " + JSON.stringify(err));
    }
}

export class AddLineArgs {
    color: Color;
    line: Polyline;
    location: Position;
    geodesic: boolean;
    width: number;
}

export class AddMarkerArgs {
    location: Position;
    title: string;
}
