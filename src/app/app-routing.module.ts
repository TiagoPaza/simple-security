import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    {path: "", redirectTo: "menu/home", pathMatch: "full"},
    {path: "menu/home", loadChildren: "~/app/menu/home/home.module#HomeModule"},
    {path: "menu/keyboard", loadChildren: "~/app/menu/keyboard/keyboard.module#KeyboardModule"},
    {path: "menu/settings", loadChildren: "~/app/menu/settings/settings.module#SettingsModule"}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {
}
